package Condiciones;
import ClasesPrincipales.*;

import ClasesPrincipales.Partido;

public class EdadMenorA30 implements Condicion {

	
	public boolean seCumpleEn(Partido unPartido) {
		
		for(Persona inscripto : unPartido.getListadoDeInscriptos()){
			if (inscripto.getEdad()>=30) return false;
		}
		return true;
		
	}
	
}
