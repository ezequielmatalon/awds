package Condiciones;
import ClasesPrincipales.Partido;

public interface Condicion {
	
	public boolean seCumpleEn(Partido unPartido);

}
