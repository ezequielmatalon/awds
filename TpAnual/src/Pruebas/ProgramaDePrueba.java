package Pruebas;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import ClasesPrincipales.Partido;
import ClasesPrincipales.Persona;
import Condiciones.EdadMenorA30;
import FormasDeInscripcion.Condicional;
import FormasDeInscripcion.Estandar;
import FormasDeInscripcion.Solidaria;

public class ProgramaDePrueba {
	Partido partido1;
	Estandar estandar;
	Solidaria solidaria;
	EdadMenorA30 edadMenorA30;
	Condicional condicionalDe30;
	Persona juan;
	Persona pepe;
	Persona pablo;
	Persona nestor;

	@Before
	public void init() {
		partido1 = new Partido(new Date(), "Calle falsa 123");
		estandar = new Estandar();
		solidaria = new Solidaria();
		edadMenorA30 = new EdadMenorA30();
		condicionalDe30 = new Condicional(edadMenorA30);
		juan = new Persona("Juan", 21, estandar);
		pepe = new Persona("Pepe", 32, estandar);
		pablo = new Persona("Pablo", 22, solidaria);
		nestor = new Persona("Nestor", 50, condicionalDe30);
		juan.inscribirseA_UnPartido(partido1);
		pepe.inscribirseA_UnPartido(partido1);
		nestor.inscribirseA_UnPartido(partido1);
		pablo.inscribirseA_UnPartido(partido1);

	}

	@Test
	public void test() {
		System.out.println(partido1.cantidadDeInscriptos());

	}

}
