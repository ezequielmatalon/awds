package FormasDeInscripcion;

import ClasesPrincipales.*;
import Condiciones.Condicion;

public class Condicional implements FormaDeInscripcion {

	private Condicion condicion; // Aca hay otro strategy con la condicion

	public void inscribirseA(Partido unPartido, Persona unaPersona) {
		if ((unPartido.tieneMenosDe10Inscriptos())
				&& (this.getCondicion().seCumpleEn(unPartido))) {
			unPartido.inscribirPersona(unaPersona);
			System.out.println("Inscripcion realizada");
		} else
			System.out.println("No se puede inscribir al partido");
	}

	public Condicional(Condicion unaCondicion) { // redefino el constructor
		condicion = unaCondicion;
	}

	// setters y getters

	public Condicion getCondicion() {
		return condicion;
	}

	public void setCondicion(Condicion condicion) {
		this.condicion = condicion;
	}

	

}
